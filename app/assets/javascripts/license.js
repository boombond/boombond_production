var existingCartId = null;
var selectedTracks = []
var currentTrack = {}
var currentObj = null
var pendingTracks=0
var pendingAmount=0

var loadExistingCart = function(){
    $.get( "/carts/latest_pending_cart.json", function( data ) {
        existingCartId = data.cart_id
        if(typeof data.pending_items != 'undefined' && parseInt(data.pending_items) >0){
            pendingTracks = data.pending_items
            $('.cart-number').removeClass('animated bounce');
            $('.cart-number').html(pendingTracks);
            $('.cart-number').addClass('animated bounce');
        }
        if(typeof data.pending_total != 'undefined' && parseInt(data.pending_total) >0){
            pendingAmount = data.pending_total
            $("#shoppingCartAmount").html(pendingAmount)
        }
            
    });
}

var checkOutCart = function(){
    $.post( "/carts/add_to_cart", { selectedTracks: selectedTracks, cart_id: existingCartId }, function( data ) {
        console.log(data)
    });

}


var getCurrentObject = function(trackId){
    for(var i=0; i < selectedTracks.length; i++){
        if(selectedTracks[i].track_id == trackId){
            return selectedTracks[i]
        }
    }
    return {}
}


var calculateCartTotal = function(){
    var sumTotal = 0
    for(var i=0; i < selectedTracks.length; i++){
        sumTotal += selectedTracks[i].licencePrice
        if(typeof selectedTracks[i]["tpt_flag"] != 'undefined' && (selectedTracks[i]["tpt_flag"] == true || selectedTracks[i]["tpt_flag"] == 'true')){
            sumTotal += selectedTracks[i].tptLicencePrice
        }
    }
    if(typeof pendingAmount != 'undifined' && pendingAmount > 0){
        sumTotal += pendingAmount;
    }
    $("#shoppingCartAmount").html(sumTotal)
       $("#plyalistSubTotal").html(sumTotal)
    return sumTotal;
}
var changeSelectedItemText = function(itemObj, id){
    $("a[rel='anchor-relation-"+id+"']").each(function(){
        $(this).find(".price-details").show();
        $(this).find(".added-to-cart").hide();
    });
    
    itemObj.find(".price-details").hide();
    itemObj.find(".added-to-cart").show();
        
    // itemObj.html('<i class="fa  fa-spinner fa-spin"></i>');
    // setTimeout(function(){
    //     if(itemObj.hasClass('small-cart')){
    //         itemObj.html('<i class="fa fa-check"></i>');
    //     }else{
    //         itemObj.html('in cart <i class="fa fa-check"></i>');  
    //     }
    // }, 2000);
}


var removeItem = function(elementId, itemId, status){
    if(status =='existing'){
        var url = "/line_items/"+itemId+".json";
        $.ajax({
            url: url,
            type: 'DELETE',
            success: function(result) {
                loadExistingCart();
                $("#"+elementId).remove();
            }
        })
    }else{
        selectedTracks.splice(itemId,1);
        $("#"+elementId).remove();
    }
    calculateCartTotal();
    
}


var plylistModel = function(){
    $.get( "/carts/latest_pending_line_items.json", function( data ) {
        console.log(data)
        var pendingLineItems = data.line_items;
        var contentStr = ""
        var total = 0
        if(typeof pendingLineItems != 'undifined' && pendingLineItems.length > 0){
            for(var i =0 ; i < pendingLineItems.length ; i++){
                var item = pendingLineItems[i]
                var liId= "LineItem"+item.id
                contentStr += "<li id='"+liId+"'>";
                contentStr += "<a href='#' class='pull-left'>"+item.title+"</a>";
                contentStr += "<a href=\"#\" class=\"pull-right\" onclick=\"removeItem('"+liId+"','"+item.id+"', 'existing')\" ><i class='fa fa-trash'></i></a>"
                contentStr += "<div class='pull-right'>$"+item.unit_price+"</div>"
                total += item.unit_price
            }
        }
        if(typeof selectedTracks != 'undifined' && selectedTracks.length > 0){
            for(var i =0 ; i < selectedTracks.length ; i++){
                var item = selectedTracks[i]
                var liId= "LineItem"+item.track_id+"_"+i
                contentStr += "<li id='"+liId+"'>";
                contentStr += "<a href='#' class='pull-left'>"+item.title+"</a>";
                contentStr += "<a href=\"#\" class=\"pull-right\" onclick=\"removeItem('"+liId+"','"+i+"', 'new')\" ><i class='fa fa-trash'></i></a>"
                contentStr += "<div class='pull-right'>$"+item.unit_price+"</div>"
                total += item.unit_price
            }
        }

        $("#plyalistSubTotal").html(total)
        $("#plyalistContentUl").html("")
        $("#plyalistContentUl").html(contentStr)
        $("#playlist").modal()
    });
        
}

$(function() {
  "use strict";
    loadExistingCart();
    $("#playlistPopup").click(function(e){
        plylistModel();
    });

    $("#shoppingCartAmountPopup").click(function(e){
        plylistModel();
    });


    $('.licensePopUp').click(function(e){
        currentObj = $(this);
        console.log(getCurrentObject($(this).data("id")))
        currentTrack = getCurrentObject($(this).data("id"))
        currentTrack["track_id"] = $(this).data("id")
        currentTrack["item_type"] = $(this).data("itemtype")
        currentTrack["licencePrice"] = parseInt($(this).data("license"))
        currentTrack["tptLicencePrice"] = parseInt($(this).data("trackoutsprice"))
        currentTrack["author"] = $(this).data("author")
        currentTrack["title"] = $(this).data("title")
        var hasTptFile = $(this).data("hastpt")
        if(hasTptFile){
            $("#trackOutPriceDisplay").show()
        }else{
            $("#trackOutPriceDisplay").hide()
        } 
        
        $(".beatMakerName").html(currentTrack["author"])
        $(".trackTitle").html(currentTrack["title"])
        $("#todaysDate").html($(this).data("today"))
        $("#licenceAuthorImg").attr("src", $(this).data("poster"));
        $("#licencePrice").html($(this).data("license"))
        $("#tptLicencePrice").html($(this).data("trackoutsprice"))
        $("#activeTrack").val($(this).data("id"))

        var total = parseInt($(this).data("license"));
        if($("#tptLicenceChecker:checked").is(':checked')){
            total = total+ parseInt($(this).data("trackoutsprice"))
        }
        currentTrack["unit_price"] = total
        currentTrack["tpt_flag"] = $("#tptLicenceChecker:checked").is(':checked')
        $("#totalPrice").html(total)
        $("#aggrementCheck").removeAttr('checked');
        $("#tptLicenceChecker").removeAttr('checked');
        $("#license-agreement").modal()
        if(typeof currentTrack["tpt_flag"] != 'undefined' && (currentTrack["tpt_flag"] == true || currentTrack["tpt_flag"] == 'true')){
            $("#tptLicenceChecker").prop("checked", true)
        }else{
            $("#tptLicenceChecker").prop("checked", false)
        }

    });
    
    $("#tptLicenceChecker").click(function(e){
        if($(this).is(':checked')){
            currentTrack["tpt_flag"] = true;
            $("#totalPrice").html((currentTrack["licencePrice"] + currentTrack["tptLicencePrice"]))
        }else{
            currentTrack["tpt_flag"] = false;
            $("#totalPrice").html(currentTrack["licencePrice"])
        }
    });

    $("#aggrementCheck").click(function(e){
        if($(this).is(':checked')){
            if(selectedTracks.indexOf(currentTrack) == -1){
                selectedTracks.push(currentTrack)
                $('.cart-number').removeClass('animated bounce');
                var cartNumber = parseInt($('.cart-number').text(),10)+1;
                $('.cart-number').html(cartNumber);
                $('.cart-number').addClass('animated bounce');
            }
            $("#checkoutButton").show()
        }else{
            
            selectedTracks = jQuery.grep(selectedTracks, function(value) {
                return value != currentTrack;
            });
            $("#checkoutButton").hide()
        }
        calculateCartTotal();
        changeSelectedItemText(currentObj, currentTrack.track_id)
    })
});
