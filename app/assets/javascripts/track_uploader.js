$(function() {
    createUploader();
});

function createUploader(){     

    var authenticity_token = $("#formAuthenticityToken").val()
    var uploader = new qq.FileUploader({
        element: document.getElementById('trackUploadNew'),
        action: '/tracks/upload_trak_file.js',
        params: { "authenticity_token": authenticity_token },
        customHeaders: { "X-File-Upload": "true" },
        allowedExtensions: ['wav'],
        uploadButtonText: "<span class='input-group-addon btn btn-default btn-file'><span class='fileinput-new'><i class='fa fa-upload'></i><br>Click here to browse or drag your file here.</span><span class='fileinput-exists'>Change</span></span>",
        //"<i class='fa fa-upload'></i><br>Click here to browse or drag your file here.</span>",
        onSubmit: function(){
            $("#submmissiont_page_container2").show();
            $("#submmissiont_page_container1").hide();
        },
        onProgress: function(id, fileName, loaded, total){
            $("#trackUploadFileName").html(fileName)
            var percent = Math.round((loaded / total) * 100);
            $("#tractUploadProgress").animate({
                width: percent+'%'
            })
            $("#tractUploadProgressNumber").html(percent + ' %')
            $("#publish_submit_button").hide();
        },
        onComplete: function(id, fileName, responseJSON){
            $(".qq-upload-failed-text").hide()
            var url = "/tracks/"+responseJSON.id+"/publish"
            $("#trackCreationForm").attr("action", url);
            createtptUploader(responseJSON.id, authenticity_token);
            $("#publish_submit_button").show();
        }
    });
}

function createtptUploader(track_id, authenticity_token){
    $("#tptUploadComments").show()
    var uploader = new qq.FileUploader({
        element: document.getElementById('tpt-file-uploader'),
        action: "/tracks/"+track_id+"/upload_tpt_file.json",
        params: { "authenticity_token":  authenticity_token},
        customHeaders: { "X-File-Upload": "true" },
        allowedExtensions: ['zip'],
        uploadButtonText: "<span class='input-group-addon btn btn-default btn-file'><span class='fileinput-new'><i class='fa fa-upload'></i><br>Click here to browse or drag your file here.</span><span class='fileinput-exists'>Change</span></span>",
        onProgress: function(id, fileName, loaded, total){
            $("#tptUploadRARName").html(fileName)
            var percent = Math.round((loaded / total) * 100);
            $("#tptUploaderPtogress").animate({
                width: percent+'%'
            })
            $("#tptUploaderProgressNumber").html(percent + ' %')
            $("#publish_submit_button").hide();
        },
        onSubmit: function(id, fileName){
            $("#tptUploadDescription").hide()
            $("#publish_submit_button").hide();
        },
        onComplete: function(id, fileName, responseJSON){
            $("#publish_submit_button").show();
            $(".tpt_row").show()
        }
    });
}

