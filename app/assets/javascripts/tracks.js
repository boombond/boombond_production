function sub_genre()
{
    $.ajax({
        type: 'GET',
        url: "/home/sub_genres",
        data: {
            'genres' : $("#track_genere").val()
        }
    }).done(function(text) {
        $('#track_subgenere').empty().html(text);
        $('#track_subgenere').selectpicker('refresh');
    });
}


$(document).ready(function(){ 
    $("#moreInfoRegion").hide();
    $("#unclearedsample").change(function(){
        $( "select option:selected").each(function(){
            if($(this).attr("value")=="Yes"){
                $(".uncleared-region").show();
            }
            if($(this).attr("value")=="No"){
                $(".uncleared-region").hide();
            }
        });
    }).change();

    $('a#addUnclearedSong').click(function(e){
        e.preventDefault()
        if(!$("#uncleared-song1").is(':visible')){
            $("#uncleared-song1").show()
            return true
        }
        if(!$("#uncleared-song2").is(':visible')){
            $("#uncleared-song2").show()
            return true
        }
        if(!$("#uncleared-song3").is(':visible')){
            $("#uncleared-song3").show()
            return true
        }
        if(!$("#uncleared-song4").is(':visible')){
            $("#uncleared-song4").show()
            return true
        }
    });
    
    
    $("#promotion").click(function () {
        if ($(this).is(":checked")) {
            $("#promotion_price").removeAttr('readonly', 'readonly');
        } else {
            $("#promotion_price").attr('readonly', 'readonly');
        }
    });


    $("#wave_promotion").click(function () {
        if ($(this).is(":checked")) {
            $("#wave_promotion_price").removeAttr('readonly', 'readonly');
        } else {
            $("#wave_promotion_price").attr('readonly', 'readonly');
        }
    });


    $("#tpt_promotion").click(function () {
        if ($(this).is(":checked")) {
            
            $("#promotion_tpt_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#promotion_tpt_price").attr('readonly', 'readonly');
        }
        
    });

    $("#track_exclusive").click(function () {

        if ($(this).is(":checked")) {
            
            $("#exclusive_licence_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#exclusive_licence_price").attr('readonly', 'readonly');
        }
        
    });

    $("#wave_exclusive").click(function () {

        if ($(this).is(":checked")) {
            
            $("#exclusive_wave_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#exclusive_wave_price").attr('readonly', 'readonly');
        }
        
    });


    $("#tpt_exclusive").click(function () {

        if ($(this).is(":checked")) {
            
            $("#exclusive_tpt_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#exclusive_tpt_price").attr('readonly', 'readonly');
        }
        
    });

    $("#track_profit").click(function () {

        if ($(this).is(":checked")) {
            
            $("#profit_licence_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#profit_licence_price").attr('readonly', 'readonly');
        }
        
    });

    $("#wave_profit").click(function () {
        
        if ($(this).is(":checked")) {
            
            $("#wave_profit_licence_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#wave_profit_licence_price").attr('readonly', 'readonly');
        }
        
    });


    $("#tpt_profit").click(function () {

        if ($(this).is(":checked")) {
            
            $("#profit_tpt_price").removeAttr('readonly', 'readonly');
        } else {
            
            $("#profit_tpt_price").attr('readonly', 'readonly');
        }
        
    });

    $("#moreInfoInvoker").click(function(){
        $("#moreInfoRegion").toggle()
    });



});

