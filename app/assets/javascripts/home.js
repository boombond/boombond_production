function fetchSatePrv()
{
    $.ajax({
        type: 'GET',
        url: "/home/state_cities",
        data: {
            'country' : $("#selectedCountry").val()
        }
    }).done(function(text) {
        $('#stateOptions').empty().html(text);
        $('#stateOptions').selectpicker('refresh');
    });
}


$(document).ready(function(){
    new WOW().init();

    $("#enableProfileImg").click(function(e){
        //$(this).hide();
        $("#profileImgDiv").show();
    })

    $("#indexAuthorDropdownLi").hover(function(){
        $("#indexAuthorDropdown").dropdown('toggle')
    })
    

});
