class HomeController < ApplicationController
  #prepend_before_filter :require_no_authentication, :only => [:update_password]



  def new
  end

  def edit
  end

  def update
   @user= User.save
  end

  def state_cities
    @country= Country.find_by_code(params[:country])
    @states = @country.states unless @country.nil?
    @str=""
    unless @states.empty?
      @str=""
      @str +="'<option value="">Select State</option>"
      @states.each do |s|
        @str += "<option value=#{s.name}>#{s.name}</option>"
      end
      @str +="'"
    end
    render :text=>@str
  end

  def dashbord
    
  end

  def social_networks
 
  end

  def settings
 
  end

  def sub_genres
    @genre= Genre.find_by_name(params[:genres])
    @subgenres = @genre.subgenres unless @genre.nil?
    @str=""
      unless @subgenres.nil?
      @str=""
      @str +="'<option value="">Select subgenres</option>"
      @subgenres.each do |s|
        @str += "<option value=#{s.name}>#{s.name}</option>"
      end
      @str +="'"
    end
    render :text=>@str
  end

  def state_codes
    @country= Country.find_by_code(params[:country])
    @states = @country.states unless @country.nil?
   @str=""
    unless @states.empty?
      @str=""
      @str +="'<option value="">Select state_codes</option>"
      @states.each do |s|
        @str += "<option value=#{s.iso}>#{s.iso}</option>"
      end
      @str +="'"
    end
    render :text=>@str
  end

  def about_us
  end
end
