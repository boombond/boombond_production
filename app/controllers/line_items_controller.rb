class LineItemsController < ApplicationController

 def create
    @product = Track.find(params[:product_id])
    unit_price = 0
    if params[:track_type] == "promotion"
    	unit_price = @product.promotion_licence_price
      if params[:promotion_tpt_flag]
        unit_price += @product.promotion_tpt_price
      end
    elsif params[:track_type] == "profit"
    	unit_price = @product.profit_licence_price
      if params[:profit_tpt_flag]
        unit_price += @product.profit_tpt_price
      end
   	elsif params[:track_type] == "exclusive"
   		unit_price = @product.exclusive_licence_price
      if params[:exclusive_tpt_flag]
        unit_price += @product.exclusive_tpt_price
      end
   	end
    
    @line_item = LineItem.create!(:cart_id => current_cart.id, :track_id => @product.id, :item_type => params[:track_type], :tpt_flag => params[:tpt_flag],:quantity => 1, :unit_price => unit_price)
    flash[:notice] = "Added #{@product.title} to cart."
    redirect_to current_carts_url
  end

  def destroy
  	@line_item = LineItem.find_by_id(params[:id])
  	@line_item.destroy
    respond_to do |format|
      format.html { redirect_to current_carts_url }
      format.json { head :no_content }
    end
  end
  
end
