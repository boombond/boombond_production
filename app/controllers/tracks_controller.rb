require 'zip'
class TracksController < ApplicationController
  before_action :set_track, only: [:show, :edit, :update, :destroy, :download, :publish, :mp3_download]
  #before_filter :get_layout
  
  
  def index
    if current_user
      @tracks = current_user.tracks.find(:all, :conditions => ["active = 1"])
    end
  end
    
  def upload_trak_file
      if env['HTTP_X_FILE_UPLOAD'] == 'true'
        @raw_file = env['rack.input']
        
        @raw_file.class.class_eval { attr_accessor :original_filename, :content_type }
        @raw_file.original_filename = env['HTTP_X_FILE_NAME']
        @raw_file.content_type = env['HTTP_X_MIME_TYPE']
      end
      @raw_file = Paperclip::StringioAdapter.new(@raw_file)
      @track = Track.new(track_file: @raw_file)
      @track.user_id = current_user.id
      @track.save
      render json: { success: true, id: @track.id }
    end
 
  def upload_tpt_file
     if env['HTTP_X_FILE_UPLOAD'] == 'true'
      @raw_file = env['rack.input']
      @raw_file.class.class_eval { attr_accessor :original_filename, :content_type }
      @raw_file.original_filename = env['HTTP_X_FILE_NAME']
      @raw_file.content_type = env['HTTP_X_MIME_TYPE']
    end
    @raw_file = Paperclip::StringioAdapter.new(@raw_file)
    @track = Track.find(params[:id])
    @track.tpt = @raw_file
    @track.save!
    render json: { success: true, id: @track.id }
  end

  def new1
    @track = Track.new
  end

  def show

    # Delayed::Job.enqueue WavToMp3Converter::Convert.new(@track.id)

      # @params         = "-q1 -b 320"
      # @current_format = File.extname(@track.track_file.url)
      # @basename       = File.basename(@track.track_file.url, @current_format)
      # @format         = "mp3"
      # parameters = []
      # parameters << @params
      # parameters << ":source"
      # parameters << ":dest"
      # parameters = parameters.flatten.compact.join(" ").strip.squeeze(" ")
      # dst = Tempfile.new([@basename, @format ? ".#{@format}" : ''])
      # Paperclip.run("lame", parameters, :source => "#{@track.track_file.url}",:dest => "#{@track.track_file.url(:mp3)}")
  end

  def download
    # send_file(@pic.picture.to_file, :type => @pic.picture_content_type, :disposition => "attachment", :filename => @pic.picture_file_name)
    send_file(Paperclip.io_adapters.for(@track.track_file).path, :type => @track.track_file_content_type, :disposition => "attachment", :filename => @track.track_file_file_name)
  end

  def mp3_download
    puts @track.inspect
    @track.increment! :download_count
    send_file(Paperclip.io_adapters.for(@track.mp3_track_file).path, :type => @track.mp3_track_file_content_type, :disposition => "attachment", :filename => @track.mp3_track_file_file_name)
  end

 # def download_zip()
 #        if !track_list.blank?
 #          file_name = "tracks.zip"
 #          t = Tempfile.new("my-temp-filename-#{Time.now}")
 #          Zip::ZipOutputStream.open(t.path) do |z|
 #            track_list.each do |track|
 #              title = track.title
 #              title += ".mp3" unless title.end_with?(".mp3")
 #              z.put_next_entry(title)
 #              z.print IO.read(track.path)
 #            end
 #          end
 #          send_file t.path, :type => 'application/zip',
 #                                 :disposition => 'attachment',
 #                                 :filename => file_name
 #          t.close
 #        end
 #      end

  
  def download_wav
     glacier = Fog::AWS::Glacier.new(aws_access_key_id: 'AKIAJXJIEWZBXESLFSLQ',aws_secret_access_key: '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')
    vault = glacier.vaults.find("beatbucks_archive").first
    job = vault.jobs.get(params[:key])
    track = Track.find(params[:id])
    track.increment! :wav_download_count
    send_data(job.get_output.body, :filename => track.track_file_file_name,:type => "audio/wav")
  end
  
  
  def download_tpt
    glacier = Fog::AWS::Glacier.new(aws_access_key_id: 'AKIAJXJIEWZBXESLFSLQ',aws_secret_access_key: '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')
    vault = glacier.vaults.find("beatbucks_tpt_archive").first
    job = vault.jobs.get(params[:key])
    track = Track.find(params[:id])
    track.increment! :tpt_download_count
    send_data(job.get_output.body, :filename => track.tpt_file_name,:type => "application/zip")
  end


  def new
    @track = Track.new
  end

  def edit
    @track = Track.find(params[:id]) 
  end

  def new_track_step2
    @track = Track.find(params[:id])
  end
  
  def create
    @track = Track.new(track_params)
    respond_to do |format|
      if @track.save
        Delayed::Job.enqueue WavToMp3Converter::Convert.new(@track.id)
        format.html { render action: 'publish', notice: 'Track was successfully created.' }
        format.json { render action: 'show', status: :created, location: @track }
      else
        format.html { render action: 'new' }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def license
    @line_item = LineItem.find(params[:li_id])
    @billing_detail = BillingDetail.find(params[:billing_detail_id])
    @unit_price = 0
    @track_type = @line_item.item_type
    @product = @line_item.track
    @track_id = @product.id
    if @track_type == "promotion"
    	@unit_price = @product.promotion_licence_price
      if @product.present? && @line_item.tpt_flag.present?
        @unit_tpt_price = @product.promotion_tpt_price
      end
    elsif @track_type == "profit"
    	@unit_price = @product.profit_licence_price
      if @product.present? && @line_item.tpt_flag.present?
        @unit_tpt_price = @product.profit_tpt_price
      end
   	elsif @track_type == "exclusive"
   		@unit_price = @product.exclusive_licence_price
      if @product.present? && @line_item.tpt_flag.present?
        @unit_tpt_price = @product.exclusive_tpt_price
      end
   	end
    respond_to do |format|
      format.html { render layout: false }
      format.pdf { 
        html = render_to_string(:action => :license, :layout => false) 
        pdf = WickedPdf.new.pdf_from_string(html) 
        send_data(pdf, 
          :filename    => "#{@track_type}-#{@product.title}.pdf", 
          :disposition => 'attachment') 
        }
    end
     
  end
  
  def publish
    respond_to do |format|
      if @track.update(track_params)
        Delayed::Job.enqueue WavToMp3Converter::Convert.new(@track.id)
        format.html { render action: 'publish', notice: 'Track was successfully Published.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    respond_to do |format|
      if @track.update(track_params)
        format.html { render action: 'publish', notice: 'Track was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @track.destroy
    respond_to do |format|
      format.html { redirect_to "/author_profile/#{current_user.id}" }
      format.json { head :no_content }
    end
  end
  def play
     @track = Track.find(params[:id])
  end

  def increment_download_ct
    @track = Track.find(params[:id])
    @track.increment! :download_count
  end
  
  def increment_play_count
    @track = Track.find(params[:id])
    @track.increment! :play_count
    render :nothing => true
  end

  private
    def set_track
      @track = Track.find(params[:id])
    end

    def track_params
      params.require(:track).permit(:title, :bpm, :genere, :subgenere, :track_image, :tags, :key, :mood1, :mood2, :comment, :promotion_licence_price, :promotion_tpt_price, :exclusive_licence_price, :exclusive_tpt_price, :profit_licence_price, :profit_tpt_price, :user_id, :download_count, :active, :track_file, :tpt, :mp3_track_file, :track_promotion, :tpt_promotion, :track_profit, :tpt_profit, :track_exclusive, :tpt_exclusive, :hook, :unclearedsample, :unclearedsong, :unclearedartist, :uncleredDescription, :unclearedsong1, :unclearedartist1, :uncleredDescription1, :unclearedsong2, :unclearedartist2, :uncleredDescription2)
    end
end
