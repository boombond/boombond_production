class GlacierDetailsController < ApplicationController
  before_action :set_glacier_detail, only: [:show, :edit, :update, :destroy]

  def index
    @glacier_details = GlacierDetail.all
  end

  def show
  end

  def new
    @glacier_detail = GlacierDetail.new
  end

  def edit
  end

  def create
    @glacier_detail = GlacierDetail.new(glacier_detail_params)

    respond_to do |format|
      if @glacier_detail.save
        format.html { redirect_to @glacier_detail, notice: 'Glacier detail was successfully created.' }
        format.json { render action: 'show', status: :created, location: @glacier_detail }
      else
        format.html { render action: 'new' }
        format.json { render json: @glacier_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @glacier_detail.update(glacier_detail_params)
        format.html { redirect_to @glacier_detail, notice: 'Glacier detail was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @glacier_detail.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @glacier_detail.destroy
    respond_to do |format|
      format.html { redirect_to glacier_details_url }
      format.json { head :no_content }
    end
  end

  private
    def set_glacier_detail
      @glacier_detail = GlacierDetail.find(params[:id])
    end
    def glacier_detail_params
      params[:glacier_detail]
    end
end
