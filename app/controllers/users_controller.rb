class UsersController < ApplicationController
  
  prepend_before_filter :require_no_authentication, :only => [:update]


  def new
    @user = User.new
  end

  def create
    
    if user.save!
      user.add_role = params[:user][:Role].parameterize('_').to_sym
      if current_user.has_role?(:producer_or_beat_maker_or_composer)
        redirect_to(:controller => 'users',:action => new_registration_path(resource_name))
      elsif  current_user.role=="Producer"
        redirect_to(:controller => 'users',:action => 'index')
      end
    end
  end

  def stored_location_for(resource)
    nil
  end

  def after_sign_in_path_for(resource)
    redirect_to :user_edit_path
    #path_to_redirect_to
  end

  def index
    @users=User.all 
    @tracks=Track.has_atleast_one_price.all 
    # redirect_to(:controller => 'tracks',:action => 'index')
  end

  def global_search
    @tracks = []
    if params[:search_term].present? 
      @tracks = Track.search(params[:search_term])
    end
    if @tracks.count <= 0
      flash.now[:error] = "No track found....!!"
    end
    render "index"
  end


  def edit
  end

  
  def  buy
  end

  def author_profile
    @user = User.find(params[:id])
  end

  def profile
  end

  def orders
  end

  def settings
    @user = current_user
  end

  def save_settings
    @user = User.find(params[:id])
    if @user.valid_password?params[:user][:current_password]
      @user.update(setting_params)
    end
  end

  def social_networks
  end

  def set_song_in_session
    respond_to do |format|
      format.json do
        track = Track.find(params[:song_id])
        data = track.attributes.merge(:artist_name => track.user.try(:ArtistName))
        data = data.merge(:mp3 => (track.mp3_track_file.present? ? track.mp3_track_file.expiring_url(24.hours): '' ))
        data = data.merge(:mp3 => (track.track_image.present? ? track.track_image.url: '' ))
        session[:current_playing_song] = data
        render :json => {:status => true}
      end
    end
  end
  private

  def setting_params
    params.require(:user).permit(:Firstname, :Lastname, :Street, :HomeTown, :Zipcode, :Country, :StateProvince, :paypalemail, :email, :password, :password_confirmation)
  end

end
