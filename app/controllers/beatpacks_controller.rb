class BeatpacksController < ApplicationController
 
  before_action :set_beatpack, only: [:show, :edit, :update, :destroy ]
  layout "user_layout"
  
  def index
    redirect_to :controller => :beatpacks, :action => :new 
  end

  def show
	  @beatpack = Beatpack.find(params[:id])
  end

  def new
    @beatpack = Beatpack.new
  end

  def edit
  
  end

  def create
    @beatpack = Beatpack.new(beatpack_params)
    respond_to do |format|
      if @beatpack.save
        format.html { render action: 'publish', notice: 'Beatpack was successfully created.' }
        format.json { render action: 'show', status: :created, location: @beatpack }
      else
        format.html { render action: 'new' }
        format.json { render json: @beatpack.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @track.update(track_params)
        format.html { redirect_to @beatpack, notice: 'Track was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @beatpack.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @beatpack.destroy
    respond_to do |format|
      format.html { redirect_to beatpacks_url }
      format.json { head :no_content }
    end
  end

  private
    
    def set_beatpack
      @beatpack = Beatpack.find(params[:id])
    end

     
    def beatpack_params
      #params.require(:beatpack).permit(:Title , :Trackscount, :Comment, :Downloadcount, :user_id )
                                   
    end
 end


 
