class PaymentNotificationsController < ApplicationController
  protect_from_forgery :except => [:create]
  
  def create
    cart = Cart.find_by_order_id(params[:invoice])
    PaymentNotification.create!(:params => params, :cart_id => cart.id, :status => params[:payment_status], :transaction_id => params[:txn_id])
       
    cart.line_items.each do |li|
      track = li.track
      Delayed::Job.enqueue GlacierWavRetrivalJob.new(track.id), :run_at => Time.now
      if track.tpt.try(:path).present?
        Delayed::Job.enqueue GlacierTptRetrivalJob.new(track.id), :run_at => Time.now
      end
    end
    Delayed::Job.enqueue PurchaseDeliveryMail.new(cart.billing_detail), :run_at => Time.now + 5.hours
    render :nothing => true
  end
end
