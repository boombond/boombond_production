class CartsController < ApplicationController
  #before_filter :get_layout

  def current
    @cart = current_cart
    if current_cart.line_items.empty?
      flash[:notice] = "Your Cart is empty to checkout"
      redirect_to root_path
    end
  end
  
  def license_agrement
    @cart = current_cart
    @track_id = params[:product_id]
    @track_type = params[:track_type]
    @product = Track.find(params[:product_id])
    @unit_price = 0
    if params[:track_type] == "promotion"
    	@unit_price = @product.promotion_licence_price
      if @product.present? && @product.promotion_tpt_price.present?
        @unit_tpt_price = @product.promotion_tpt_price
      end
    elsif params[:track_type] == "profit"
    	@unit_price = @product.profit_licence_price
      if @product.present? && @product.profit_tpt_price.present?
        @unit_tpt_price = @product.profit_tpt_price
      end
   	elsif params[:track_type] == "exclusive"
   		@unit_price = @product.exclusive_licence_price
      if @product.present? && @product.exclusive_tpt_price.present?
        @unit_tpt_price = @product.exclusive_tpt_price
      end
    end
    
  end
  
  def billing_payments
    @cart = current_cart
    if current_cart.line_items.empty?
      flash[:notice] = "Your Cart is empty to checkout"
      redirect_to root_path
    end
  end
  
  def billing_payments2
    if current_cart.line_items.empty?
      flash[:notice] = "Your Cart is empty to checkout"
      redirect_to root_path
    end
    @billing_detail = BillingDetail.new
    if current_user.present?
      @billing_detail.first_name = current_user.Firstname
      @billing_detail.last_name = current_user.Lastname
      @billing_detail.email = current_user.email
      @billing_detail.country = current_user.Country
      # @billing_detail.user_id = 1
    end
    @cart = current_cart
  end
  
  def complete_payment
   @billing_detail = BillingDetail.new(params[:billing_detail])
    # @billing_detail.user_id = 1
    @billing_detail.cart_id = current_cart.id
    if @billing_detail.invalid?
      flash[:notice] = "Please Fill All Details Below"
      redirect_to billing_payments2_carts_path
    
  else
    @billing_detail.save!
  
    @cart = current_cart
    pay_pal_url = @cart.paypal_url(payment_done_carts_url(cart_id: @cart.id), payment_notifications_url)
    redirect_to pay_pal_url
  end
  end
  
  def payment_done
    @cart = Cart.find(params[:cart_id]) 
    @cart.status = 'completed'
    @cart.save
    render "download"
  end

  def billing
    @cart = Cart.find(params[:id])
    @line_items = @cart.line_items
    @total_value = @line_items.inject(0){|sum, li| sum+li.unit_price}
    @billing_details = BillingDetail.new
    if current_user.present?
      @billing_details.user_id = current_user.id
      @billing_details.first_name = current_user.Firstname
      @billing_details.last_name = current_user.Lastname
      @billing_details.email = current_user.email
      @billing_details.cart_id = @cart.id
    end
  end

  def save_billing
    @billing_detail = BillingDetail.new(billing_detail_params)
    if @billing_detail.save
      redirect_to download_cart_path(@billing_detail.cart)
    else
      render "billing"
    end
  end
  
  def download
    @cart = Cart.find(params[:id])
  end
  
  def checkout
    @cart = Cart.find(params[:id])
    @line_items = @cart.line_items
    @total_value = @line_items.inject(0){|sum, li| sum+li.unit_price}
  end
  
  def add_to_cart
    if params[:cart_id].present?
      session[:cart_id] = params[:cart_id] 
    end
    @cart = current_cart
    if params[:selectedTracks].present?
      unless @cart.present?
        @cart = Cart.new do |cart|
          cart.purchased_at = Date.today
          cart.status = 'pending'
        end
      end
      ActiveRecord::Base.transaction do
        @toal_value = 0
        if @cart.save
          @line_items = params[:selectedTracks].map do |key, val|
            LineItem.new({:track_id => val[:track_id], :item_type => val[:item_type], :tpt_flag => val[:tpt_flag]}) do |li|
              li.cart_id = @cart.id
              if li.tpt_flag
                li.unit_price = val[:licencePrice].to_i + val[:tptLicencePrice].to_i
              else
                li.unit_price = val[:licencePrice].to_i
              end
              
            end
          end
          if(@line_items.map(&:save).all?)
            render :js => "window.location = '#{checkout_cart_path(@cart)}'"
          end
        else
          raise ActiveRecord::Rollback
        end
      end
    elsif @cart.line_items.count > 0
      render :js => "window.location = '#{checkout_cart_path(@cart)}'"
    else
      render :js => "window.location = '#{root_path}'"
    end
  end
  
  def latest_pending_cart
    respond_to do |format|
      format.json do
        pending_cart = current_user.present? ? Cart.belongs_to_user(current_user).where(:status => "pending").last : nil
        if pending_cart.present?
          pending_items = pending_cart.line_items.count
          pending_total = pending_cart.line_items.inject(0){|sum, item| sum+item.unit_price}
          render :json => {:cart_id => pending_cart.id, :pending_items => pending_items, :pending_total => pending_total}
        else
          render :json => {:cart_id => nil}
        end
      end
    end
  end

  def latest_pending_line_items
    respond_to do |format|
      format.json do
        pending_cart = current_user.present? ? Cart.belongs_to_user(current_user).where(:status => "pending").last : nil
        if pending_cart.present?
          pending_items = pending_cart.line_items.map{|li| {:id => li.id, :title => li.track.title, :unit_price => li.unit_price} }
          render :json => {:line_items => pending_items}
        else
          render :json => {:line_items => []}
        end
      end
    end

  end
  

  private

  def billing_detail_params
    params.require(:billing_detail).permit(:first_name, :last_name, :artist_name, :email, :cart_id, :user_id)
  end
end
