 class RegistrationsController < Devise::RegistrationsController

   skip_before_filter :require_no_authentication

    def create
      @user = User.new(params[:user])
    	if @user.save
          @user.add_role(params[:user][:role])
          redirect_to(:controller => 'users',:action => 'index')
          flash[:success] = "Registration has been completed successfully \n Please Check your Email \n and confirm account"
        else
          flash[:fail] = "Registration has not been completed successfully \n Please fix th errors"
        render action: 'new'
        end
    end

    def update
      @user.update_without_password(devise_parameter_sanitizer.sanitize(:account_update))
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
      end
      @user = User.find(current_user.id)
      if @user.update_attributes(params[:user])
        if params[:user][:role]
          @user.update_role(params[:user][:role])
        end
        flash.now[:success] = "Profile has been updated successfully"
        sign_in @user, :bypass => true
        if params[:user][:update_call_from].present? and params[:user][:update_call_from] == 'settings'
          redirect_to settings_path
        else
          redirect_to edit_registration_path(resource)
        end
      else
        if params[:user][:update_call_from].present? and params[:user][:update_call_from] == 'settings'
          render "users/settings"
        else
          render "edit"
        end
      end
    end

    protected

    def after_update_path_for(resource)
    end

    def after_sign_up_path_for(resource)
      user_path(resource)
    end

end
