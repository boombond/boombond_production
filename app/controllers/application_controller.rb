class ApplicationController < ActionController::Base
  before_action :configure_devise_permitted_parameters, if: :devise_controller?
  
  def current_cart
    if session[:cart_id].present?
      unless @current_cart.present? 
        @current_cart ||= Cart.find(session[:cart_id])
      end
      if @current_cart.id.to_s != session[:cart_id]
        @current_cart = Cart.find(session[:cart_id])
      end
      #@current_cart ||= Cart.find(session[:cart_id])
      #session[:cart_id] = nil if @current_cart.purchased_at
    else
      @current_cart = Cart.new
      @current_cart.status = "pending"
      @current_cart.save!
      session[:cart_id] = @current_cart.id
    end
    @current_cart
  end

  def get_layout
    if @current_cart
      self.class.layout "user_layout"
    else
      self.class.layout "user_layout"
    end
  end
   
 private

def configure_devise_permitted_parameters
  registration_params = [:Country_B,:StateProvince_B,:City_B,:reset_password_token, 
                           :profile_img, :header_img, :ArtistName,:Zipcode,:paypalemail,
                           :Firstname,:Lastname,:Country,:Howdouknow,:Terms, :email,:Role,
                           :provider,:uid,:password, :password_confirmation,:About,:Street,
                           :HomeTown,:StageName,:StateProvince,:Artist_blog,:Facebook,
                           :Twitter,:MySpace,:Soundcloud,:GooglePlus,:YouTube,:Soundclick,
                           :Instagram,:delete_profileimage,:state_code ]
    
        if params[:action] == 'update'
          devise_parameter_sanitizer.for(:account_update) { 
            |u| u.permit(registration_params << :current_password )
            
          }
           else params[:action] == 'create'
            devise_parameter_sanitizer.for(:sign_up) { 
            |u| u.permit(registration_params) 
             
          }
        end
end

    def after_sign_in_path_for(resource)
      if request.referrer == billing_payments_carts_url
        billing_payments2_carts_path
      else
        request.env['omniauth.origin'] || stored_location_for(resource) || root_path
      end
    
    end


  def after_sign_up_path_for(resource)
     confirmation_path
  end
  
    
end







