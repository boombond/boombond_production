class BillingDetail < ActiveRecord::Base
  belongs_to :cart
  attr_accessible :first_name,:last_name,:email,:country,:create_user, :cart_id, :user_id, :artist_name
  validates :first_name,:last_name,:email, presence: true
end
