class Track < ActiveRecord::Base
  has_many :glacier_details
  attr_accessible :title, :bpm, :genere, :subgenere, :tags, :track_image, :key, :mood1, :mood2, :comment, :promotion_licence_price, :promotion_tpt_price, :exclusive_licence_price, :exclusive_tpt_price, :profit_licence_price, :profit_tpt_price, :user_id, :download_count, :active, :track_file, :tpt, :mp3_track_file, :track_promotion, :tpt_promotion, :track_profit, :tpt_profit, :track_exclusive, :tpt_exclusive, :hook, :unclearedsample, :unclearedsong, :unclearedartist, :uncleredDescription, :unclearedsong1, :unclearedartist1, :uncleredDescription1, :unclearedsong2, :unclearedartist2, :uncleredDescription2
  belongs_to :user
  has_attached_file :track_file

  has_attached_file :mp3_track_file,:storage => :s3,s3_storage_class: :reduced_redundancy
  validates_attachment_content_type :track_file, :content_type => [ 'audio/mpeg', "audio/x-wav","audio/wav"], :message => "not a supported type, use WAV only", :if => Proc.new { |t| !t.track_file_file_name.blank? }
  # has_attached_file :track_file, :styles => {
  # 										   :mp3 => { 
  # 										     :params => "-q1 -b 320",
  # 										     :format => "mp3" }
  # 										  },
  # 										  :processors => [:wav_mp3]

  has_attached_file :tpt,:path => ":rails_root/public/system/tpt/:id/:style/:basename.:extension", :storage => :filesystem
  
  # validates_attachment_content_type :track_file, :content_type => [ 'audio/mpeg', 'audio/x-mpeg', 'audio/mp3', 'audio/x-mp3', 'audio/mpeg3', 'audio/x-mpeg3', 'audio/mpg', 'audio/x-mpg', 'audio/x-mpegaudio' ], :message => "not a supported type, use WAV only", :if => Proc.new { |t| !t.track_file_file_name.blank? }
  validates_attachment_content_type :tpt, :content_type => "application/zip", :message => "not a supported type, use ZIP only", :if => Proc.new { |t| !t.tpt_file_name.blank? }
  
  has_attached_file :track_image, :styles => {:medium => ['180x180#',  :jpg, :quality => 80], :thumb => ['100x100#',  :jpg, :quality => 70]}, :default_url => "/assets/albumimage.jpg"
  #validates_attachment_content_type :track_image, :content_type => /\Aimage\/.*\Z/
  do_not_validate_attachment_file_type :track_image


  scope :has_atleast_one_price, -> { where("promotion_licence_price > 0 OR exclusive_licence_price > 0 OR profit_licence_price > 0")}
scope :active, -> { where(:active => true) }


  def tpt_glacier_detail
    glacier_details.where(:vault_id => 'beatbucks_tpt_archive').first
  end
  
  def wav_glacier_detail
    glacier_details.where(:vault_id => 'beatbucks_archive').first
  end
  
  def tpt_glacier_key
    return tpt_glacier_detail.job_id
  end
  
  def wav_glacier_key
    return wav_glacier_detail.job_id
  end


  def self.search(term)
    self.where("lower(title) LIKE '%#{term.downcase}%' || lower(tags) LIKE '%#{term.downcase}%' || user_id in (?)", User.select(:id).matching_with_name(term.downcase))
  end
  
  private
  # def convert_mp3
  #   system("ffmpeg -i #{self.track_file.to_file.path} -vn -ar 44100 -ac 2 -ab 64 -f mp3 #{track_file.to_file.path}")
  # end
end
