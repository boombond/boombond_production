class LineItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :track
  attr_accessible  :unit_price, :quantity,:cart_id,:track_id,:item_type,:tpt_flag
  
  def full_price
    unit_price.to_i * quantity.to_i
  end
  
end
