class Cart < ActiveRecord::Base
  has_many :line_items
  has_one :billing_detail
  attr_accessible  :purchased_at
  accepts_nested_attributes_for :line_items

  scope :belongs_to_user, lambda{|user| where(:id => BillingDetail.select(:cart_id).where(:user_id => user.id))}
  scope :completed, lambda{ where(:status => "completed")}

  def total_price
    line_items.to_a.sum(&:full_price)
  end
  
  def paypal_url(return_url, notify_url)
    order_id = Time.now.to_i
    values = {
      :business => AppConfig.paypal_email,
      :cmd => '_cart',
      :upload => 1,
      :return => return_url,
      :invoice => order_id,
      :notify_url => notify_url
    }
    
    line_items.each_with_index do |item, index|
      values.merge!({
        "amount_#{index+1}" => item.unit_price,
        "item_name_#{index+1}" => item.track.title,
        "item_number_#{index+1}" => item.id,
        "quantity_#{index+1}" => item.quantity
      })
    end
    self.order_id = order_id
    self.save
    ret_url = AppConfig.paypal_url + values.to_query
    return ret_url
  end
end
