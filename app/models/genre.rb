class Genre < ActiveRecord::Base
has_many :subgenres,dependent: :destroy
attr_accessible :name
end