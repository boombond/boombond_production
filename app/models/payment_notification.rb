class PaymentNotification < ActiveRecord::Base
  belongs_to :cart
  serialize :params
  after_create :mark_cart_as_purchased
  
  attr_accessible :params,:purchased_at,:cart_id,:status,:transaction_id
  private
  
  def mark_cart_as_purchased
    if status == "Completed"
      self.cart.update_attribute(:purchased_at, Time.now)
    end
  end
end
