class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  simple_roles
  attr_accessor :role  
  attr_accessible :Country_B,:StateProvince_B,:City_B,:reset_password_token, :profile_img, :header_img, :Zipcode,:remember_me,:state_code,:paypalemail,:delete_profileimage,:ArtistName,:Firstname,:Lastname,:Country,:Howdouknow,:Terms, :email, :password,:role,:provider,:uid,:password, :password_confirmation,:About,:Street,:HomeTown,:StageName,:StateProvince,:Artist_blog,:Facebook,:Twitter,:MySpace,:Soundcloud,:GooglePlus,:YouTube,:Soundclick,:Instagram
  attr_accessor :update_call_from

  has_many :tracks
  has_many :beatpacks,dependent: :destroy
  has_many :user_roles
  has_many :roles, :through => :user_roles
  has_many :billing_details
  # after_save :add_roles
 
  has_attached_file :profile_img
  
  do_not_validate_attachment_file_type :profile_img

  has_attached_file :header_img
  
  do_not_validate_attachment_file_type :header_img
 
  devise :database_authenticatable, :registerable,:confirmable, :recoverable, :rememberable, :trackable, :omniauthable,:validatable,:omniauth_providers => [:facebook ]

  validates :Firstname, :Lastname, length: { maximum:25 } 
  validates_format_of :email,:with => Devise.email_regexp

  before_validation :clear_profileimage

  scope :matching_with_name, lambda{|term| where("lower(Firstname) LIKE '%#{term}%' || lower(Lastname) LIKE '%#{term}%' || lower(ArtistName) LIKE '%#{term}%'")}

  def tracks_ordered
    Cart.belongs_to_user(self).completed.map(&:line_items).flatten
  end
  
  def after_businessinfo
    User.update(:paypalemail => "#{self.email}",:Country_B=> "#{self.Country}",:StateProvince_B=> "#{self.StateProvince}",)
  end

  def add_role(role)
    ur = UserRole.new
    ur.role_id = role
    ur.user_id = self.id
    ur.save
  end

  def update_role(role)
    self.user_roles.destroy_all
    ur = UserRole.new
    ur.role_id = Role.find_by_name(role).id
    ur.user_id = self.id
    ur.save
  end

  def address
    "#{self.HomeTown}, #{self.StateProvince}, #{self.Country}"
  end


  def name
    name = ""
    if self.Firstname.present?
      name += self.Firstname
    end
    if self.Lastname.present?
      name += " "+self.Lastname
    end
  end

  def has_role?(role)
    self.roles.select{ |r| role == r }.size > 0
  end

  def add_roles
    self.add_role = self.Role.parameterize('_').to_sym
  end
 
  def delete_profileimage=(value)
    @delete_profileimage = !value.to_i.zero?
  end

  def can_upload_track?
      first_role.name == 'Beatmaker/Producer/Arranger' if first_role.present?
  end

  def first_role
    @fist_role ||= self.user_roles.first.try(:role)
  end
  
  def delete_profileimage
    !!@delete_profileimage
  end

  alias_method :delete_profileimage?, :delete_profileimage
  
  def clear_profileimage
    self.profileimage = nil if delete_profileimage?
  end

    def something_was_checked
    if self.Terms.blank?
      self.errors.add(:Terms, "Should Agree .. Please click the terms button ")
    end
  end

  def email_regexp
    if email.present? and email.match (/\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i)    
      errors.add :email, "This is not a valid email format"
      end
  end 


  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    if user
      return user
    else
      registered_user = User.where(:email => auth.info.email).first
      if registered_user
        return registered_user
      else
        user = User.create( Firstname:auth.extra.raw_info.name,
                            Lastname:auth.extra.raw_info.last_name,
                            Country:auth.info.city,
                            provider:auth.provider,
                            uid:auth.uid,
                            email:auth.info.email,
                            password:Devise.friendly_token[0,20],

                          )
      end     
    end
  end
end

