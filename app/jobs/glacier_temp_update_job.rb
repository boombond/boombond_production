class GlacierTempUpdateJob < Struct.new(:glacier_id)
  
  def perform
    obj = GlacierDetail.find(glacier_id)
    if obj.temp_job_id
      obj.status = "Download"
      obj.job_id = obj.temp_job_id
      obj.temp_job_id = ''
      obj.save!
    end
  end

end