json.array!(@glacier_details) do |glacier_detail|
  json.extract! glacier_detail, :id
  json.url glacier_detail_url(glacier_detail, format: :json)
end
