require 'zip'
class WavToMp3Converter < ActionMailer::Base

  def process_track_files(track)
    puts "Background Job Started for track #{track.id} Start"
    if track.track_file.present?
      upload_mp3_to_glacier(track)
    end
    if track.tpt.try(:path).present?
      upload_tpt_to_glacier(track)
    end
    puts "Background Job Started for track #{track.id} End"
  end
  
  
  def upload_mp3_to_glacier(track)
    @temp_file = ""
    track.mp3_track_file = convert_mp3(track)
    if track.save!
      puts "MP3 File copied To s3"
      tempfile = Paperclip.io_adapters.for(track.track_file)
      glacier = Fog::AWS::Glacier.new(aws_access_key_id: 'AKIAJXJIEWZBXESLFSLQ',aws_secret_access_key: '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')
      vault = glacier.vaults.find("beatbucks_archive").first || glacier.vaults.create( id: "beatbucks_archive")
      archive = vault.archives.create :body => tempfile, :multipart_chunk_size => 1024*1024 
      if archive
        puts "WAV File  Upload to Glacier"
        glacier_det = GlacierDetail.new
        glacier_det.track_id = track.id
        glacier_det.vault_id = "beatbucks_archive"
        glacier_det.file_name = track.track_file_file_name
        glacier_det.archive_id = archive.id 
        glacier_det.archive_at = Time.new
        glacier_det.save
        Delayed::Job.enqueue GlacierWavRetrivalJob.new(track.id), :run_at => Time.now + 24.hours
        if track.track_file.destroy
          puts "WAV File  Deleted From s3"
        end
      end
    end
  end
  
  def upload_tpt_to_glacier(track)
    puts "TPT File uploading process started"
    glacier = Fog::AWS::Glacier.new(aws_access_key_id: 'AKIAJXJIEWZBXESLFSLQ',aws_secret_access_key: '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')
    vault = glacier.vaults.find("beatbucks_tpt_archive").first || glacier.vaults.create( id: "beatbucks_tpt_archive")
    archive = vault.archives.create :body => File.read(track.tpt.path), :multipart_chunk_size => 1024*1024
    if archive
        glacier_det = GlacierDetail.new
        glacier_det.track_id = track.id
        glacier_det.vault_id = "beatbucks_tpt_archive"
        glacier_det.file_name = track.tpt_file_name
        glacier_det.archive_id = archive.id 
        glacier_det.archive_at = Time.new
        glacier_det.save!
        Delayed::Job.enqueue GlacierTptRetrivalJob.new(track.id), :run_at => Time.now + 24.hours
        puts "TPT upload success"
        
        #deleting physical file from uplaods folder
        File.delete(track.tpt.path)
      end
    puts "TPT File uploading process completed"
  end
  

  class Convert < Struct.new(:track_id)
    def perform
      track = Track.find_by_id(track_id)
      if track.tpt.try(:path).present?
        tempfile = Paperclip.io_adapters.for(track.tpt)
        track.tpt_file_count = Zip::File.open(tempfile.path).count
        track.save!
      end
      if track.track_file.try(:path).present? || track.tpt.try(:path).present?
        WavToMp3Converter.process_track_files(track).deliver
      end
      track.active = 1
      track.save
    end 
    
    def max_attempts
      return 1
    end
  end

  private

    def convert_mp31(track)
      system("avconv -i #{track.track_file.url} -f mp3 op.mp3")
    end


    def reconvert_to_mp3
      wavfile = Tempfile.new(".wav")
      wavfile.binmode
      open(wav.url) do |f|
        wavfile << f.read
      end
      wavfile.close
      convert_tempfile(wavfile)
    end

    def convert_mp3(track)
      #tempfile = Paperclip.io_adapters.for(track.track_file)
      tempfile = Paperclip.io_adapters.for(track.track_file)
      filename = File.basename(track.track_file_file_name, File.extname(track.track_file_file_name))

      unless tempfile.nil?
        return convert_tempfile(tempfile, filename)
      end
      return track.track_file
    end

    def convert_tempfile(tempfile, filename="track")
      dst = Tempfile.new([filename,".mp3"], :encoding => 'utf-8')
      cmd_args = [tempfile.path, File.expand_path(dst.path)]
      if !system("lame", *cmd_args)
        cmd_args = ["--mp3input",tempfile.path, File.expand_path(dst.path)]
        system("lame", *cmd_args)
      end  
      dst.binmode
      io = StringIO.new(dst.read)
      io.class.class_eval { attr_accessor :original_filename, :content_type }
      dst.close
      io.original_filename = filename+".mp3"
      io.content_type = "audio/mpeg"
      return io
    end

end