class UserMailer < ActionMailer::Base

  default from: "team@boombond.com"
  
  def send_wav_file_mail(track_id, job_id, billing_detail)
    @track = Track.find(track_id)
    @key = job_id
    mail(:to => "Boombond <team@boombond.com>", :subject => "Purchace Delivery")
  end
  
  def purchase_delivery(billing_detail)
    @billing_detail = billing_detail
    @cart = billing_detail.cart
    @user_present = User.find_by_email(billing_detail.email).present? ? true : false
    mail(:to => "#{billing_detail.first_name} <#{billing_detail.email}>", :subject => "Purchace Delivery")
  end
  
  def send_tpt_file_mail(track_id, job_id)
    @track = Track.find(track_id)
    @key = job_id
    mail(:to => "Boombond <team@boombond.com>", :subject => "Purchace Delivery")
  end
  
end