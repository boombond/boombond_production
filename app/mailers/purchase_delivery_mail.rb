class PurchaseDeliveryMail < Struct.new(:billing_detail)

  def perform
     UserMailer.purchase_delivery(billing_detail).deliver
  end 
    
    def max_attempts
      return 1
    end
    
  
end