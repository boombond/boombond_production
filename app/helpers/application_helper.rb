module ApplicationHelper

  def get_country_state_options(country_code)
    country_code.present? ? Country.find_by_code(country_code).states.map{|s| [s.name, s.name]} : [[]]
  end
  
  def roles
    rls = []
    SimpleRoles::Configuration.valid_roles.each do |role|
      rls << role.to_s.gsub("_"," ").capitalize
    end
    return rls
  end
  
  def promotion_dos_dont_text
    str = "License for one (1) non-profit use only.<br/><br/>"
    str += "<div class='pull-left'>Dos: </div>Alter <br/>"
    str += "Record a song<br/>"
    str += "Make a music video<br/>"
    str += "Perform at events<br/>"
    str += "Give Copies<br/><br/>"
    str += "<div class='pull-left'>Dont's: </div> Sell Copies<br/>"
    str += "Use in advertising<br/>"
    str += "Associate to a brand<br/>"
    return str
  end
  
  
  def profit_dos_dont_text
    str = "License for one (1) non-profit use only.<br/><br/>"
    str += "<div class='pull-left'>Dos: </div>Alter <br/>"
    str += "Record a song<br/>"
    str += "Make a music video<br/>"
    str += "Perform at events<br/>"
    str += "Give Copies<br/><br/>"
    str += "<div class='pull-left'>Dont's: </div> Sell Copies<br/>"
    str += "Use in advertising<br/>"
    str += "Associate to a brand<br/>"
    return str
  end
  
  def exclusive_dos_dont_text
    str = "License for one (1) non-profit use only.<br/><br/>"
    str += "<div class='pull-left'>Dos: </div>Alter <br/>"
    str += "Record a song<br/>"
    str += "Make a music video<br/>"
    str += "Perform at events<br/>"
    str += "Give Copies<br/><br/>"
    str += "<div class='pull-left'>Dont's: </div> Sell Copies<br/>"
    str += "Use in advertising<br/>"
    str += "Associate to a brand<br/>"
    return str
  end

  def flash_alert_class(key)
    key = 'danger' if key == :error or key == :alert
    key = 'success' if key == "notice"
    alert_class = ["alert"]
    if key.to_s == "fail"
      alert_class << "alert-danger"
    elsif key == :notice
      alert_class << "alert-info"
    else
      alert_class << "alert-#{key}"
    end
    alert_class.join(" ")
  end


  
end
