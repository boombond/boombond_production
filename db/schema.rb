# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150731014735) do

  create_table "beatpacks", force: true do |t|
    t.string   "Title"
    t.integer  "Trackscount"
    t.text     "Comment"
    t.integer  "Downloadcount"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "billing_details", force: true do |t|
    t.integer  "user_id"
    t.text     "first_name"
    t.text     "last_name"
    t.text     "artist_name"
    t.text     "country"
    t.text     "email"
    t.boolean  "create_user"
    t.integer  "cart_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "carts", force: true do |t|
    t.datetime "purchased_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "order_id"
    t.string   "status",       default: "approved"
  end

  create_table "countries", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "genres", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "glacier_details", force: true do |t|
    t.integer  "track_id"
    t.text     "file_name"
    t.datetime "starts_at"
    t.text     "archive_id"
    t.string   "vault_id"
    t.boolean  "download_flag"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "job_id"
    t.datetime "archive_at"
    t.text     "temp_job_id"
  end

  create_table "line_items", force: true do |t|
    t.decimal  "unit_price", precision: 10, scale: 0
    t.integer  "track_id"
    t.integer  "cart_id"
    t.integer  "quantity"
    t.string   "item_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "tpt_flag"
  end

  create_table "payment_notifications", force: true do |t|
    t.text     "params"
    t.integer  "cart_id"
    t.string   "status"
    t.string   "transaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "producer_profiles", force: true do |t|
    t.string   "Street"
    t.string   "HomeTown"
    t.string   "StateProvince"
    t.integer  "Zipcode"
    t.text     "About"
    t.string   "Artist_blog"
    t.string   "Facebook"
    t.string   "Twitter"
    t.string   "MySpace"
    t.string   "Soundcloud"
    t.string   "GooglePlus"
    t.string   "YouTube"
    t.string   "Soundclick"
    t.string   "Instagram"
    t.integer  "users_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name"], name: "index_roles_on_name", unique: true, using: :btree

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "states", force: true do |t|
    t.string   "name"
    t.string   "iso"
    t.integer  "country_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subgenres", force: true do |t|
    t.string   "name"
    t.integer  "genre_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tracks", force: true do |t|
    t.string   "title"
    t.string   "bpm"
    t.string   "genere"
    t.string   "subgenere"
    t.string   "tags"
    t.string   "key"
    t.string   "mood1"
    t.string   "mood2"
    t.text     "comment"
    t.integer  "promotion_licence_price"
    t.integer  "promotion_tpt_price"
    t.integer  "exclusive_licence_price"
    t.integer  "exclusive_tpt_price"
    t.integer  "profit_licence_price"
    t.integer  "profit_tpt_price"
    t.integer  "user_id"
    t.integer  "download_count"
    t.integer  "published"
    t.boolean  "active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "track_file_file_name"
    t.string   "track_file_content_type"
    t.integer  "track_file_file_size"
    t.datetime "track_file_updated_at"
    t.string   "tpt_file_name"
    t.string   "tpt_content_type"
    t.integer  "tpt_file_size"
    t.datetime "tpt_updated_at"
    t.string   "unclearedsample"
    t.string   "unclearedsong"
    t.string   "unclearedartist"
    t.string   "uncleredDescription"
    t.string   "unclearedsong1"
    t.string   "unclearedartist1"
    t.string   "uncleredDescription1"
    t.string   "unclearedsong2"
    t.string   "unclearedartist2"
    t.string   "uncleredDescription2"
    t.string   "unclearedsong3"
    t.string   "unclearedartist3"
    t.string   "uncleredDescription3"
    t.string   "unclearedsong4"
    t.string   "unclearedartist4"
    t.string   "uncleredDescription4"
    t.string   "mp3_track_file_file_name"
    t.string   "mp3_track_file_content_type"
    t.integer  "mp3_track_file_file_size"
    t.datetime "mp3_track_file_updated_at"
    t.integer  "tpt_files_count",             default: 0
    t.boolean  "track_promotion"
    t.boolean  "tpt_promotion"
    t.boolean  "track_exclusive"
    t.boolean  "tpt_exclusive"
    t.boolean  "track_profit"
    t.boolean  "tpt_profit"
    t.boolean  "hook"
    t.integer  "tpt_file_count"
    t.integer  "play_count"
    t.integer  "wav_download_count"
    t.integer  "tpt_download_count"
    t.string   "track_image_file_name"
    t.string   "track_image_content_type"
    t.integer  "track_image_file_size"
    t.datetime "track_image_updated_at"
    t.boolean  "is_non_profit_wave"
    t.integer  "non_profit_wave_price"
    t.boolean  "is_exclusive_wave"
    t.integer  "exclusive_wave_price"
    t.boolean  "is_non_exclusive_wave"
    t.integer  "non_exclusive_wave_price"
    t.string   "sample_rate"
  end

  add_index "tracks", ["tags"], name: "index_tracks_on_tags", using: :btree
  add_index "tracks", ["title"], name: "index_tracks_on_title", using: :btree

  create_table "user_roles", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "role_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_roles", ["user_id", "role_id"], name: "index_user_roles_on_user_id_and_role_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                     default: "", null: false
    t.string   "encrypted_password",        default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",             default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "Firstname"
    t.string   "Lastname"
    t.string   "Country"
    t.boolean  "Terms"
    t.string   "ArtistName"
    t.string   "Street"
    t.string   "HomeTown"
    t.string   "StageName"
    t.string   "StateProvince"
    t.integer  "Zipcode"
    t.text     "About"
    t.string   "Artist_blog"
    t.string   "Facebook"
    t.string   "Twitter"
    t.string   "MySpace"
    t.string   "Soundcloud"
    t.string   "GooglePlus"
    t.string   "YouTube"
    t.string   "Soundclick"
    t.string   "Instagram"
    t.string   "country_code"
    t.string   "state_code"
    t.string   "confirmation_token"
    t.string   "unconfirmed_email"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "paypalemail"
    t.string   "profileimage_file_name"
    t.string   "profileimage_content_type"
    t.integer  "profileimage_file_size"
    t.datetime "profileimage_updated_at"
    t.string   "Country_B"
    t.string   "StateProvince_B"
    t.string   "City_B"
    t.string   "profile_img_file_name"
    t.string   "profile_img_content_type"
    t.integer  "profile_img_file_size"
    t.datetime "profile_img_updated_at"
    t.string   "header_img_file_name"
    t.string   "header_img_content_type"
    t.integer  "header_img_file_size"
    t.datetime "header_img_updated_at"
  end

  add_index "users", ["ArtistName"], name: "index_users_on_ArtistName", using: :btree
  add_index "users", ["Firstname"], name: "index_users_on_Firstname", using: :btree
  add_index "users", ["Lastname"], name: "index_users_on_Lastname", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
