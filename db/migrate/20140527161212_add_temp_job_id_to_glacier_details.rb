class AddTempJobIdToGlacierDetails < ActiveRecord::Migration
  def change
    add_column :glacier_details, :temp_job_id, :text
  end
end
