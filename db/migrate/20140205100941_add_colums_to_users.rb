class AddColumsToUsers < ActiveRecord::Migration
  def change
	add_column :users, :Firstname, :string
  	add_column :users, :Lastname, :string
  	add_column :users, :Country, :string
    	 #add_column :users, :Role, :string
    	add_column :users, :Terms, :boolean
	add_column :users, :ArtistName, :string
	add_column :users, :Street, :string
	add_column :users, :HomeTown, :string
	add_column :users, :StageName, :string
    	add_column :users, :StateProvince, :string
    	add_column :users, :Zipcode, :integer
    	add_column :users, :About, :text
    	add_column :users, :Artist_blog, :string
    	add_column :users, :Facebook, :string
    	add_column :users, :Twitter, :string
    	add_column :users, :MySpace, :string
    	add_column :users, :Soundcloud, :string
    	add_column :users, :GooglePlus, :string
    	add_column :users, :YouTube, :string
    	add_column :users, :Soundclick, :string
    	add_column :users, :Instagram, :string
    	add_column :users, :country_code, :string
    	add_column :users, :state_code, :string
	add_column :users, :confirmation_token, :string
    	add_column :users, :unconfirmed_email, :string
    	add_column :users, :confirmed_at, :datetime
    	add_column :users, :confirmation_sent_at, :datetime

  end
end
