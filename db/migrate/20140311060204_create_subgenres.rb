class CreateSubgenres < ActiveRecord::Migration
  def change
    create_table :subgenres do |t|
      t.string :name
      t.integer :genre_id
      t.timestamps
    end


  end
end