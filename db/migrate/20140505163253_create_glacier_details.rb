class CreateGlacierDetails < ActiveRecord::Migration
  def self.up
    create_table :glacier_details, :force => true do |table|
      table.integer  :track_id 
      table.text     :file_name                 
      table.datetime :starts_at                                  
      table.text     :archive_id
      table.string   :vault_id
      table.boolean  :download_flag
      table.string   :status
      table.timestamps
    end
  end

  def self.down
    drop_table :glacier_details
  end
end
