class AddWaveLicenceAndPricingFieldsToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :is_non_profit_wave, :boolean
    add_column :tracks, :non_profit_wave_price, :integer
    add_column :tracks, :is_exclusive_wave, :boolean
    add_column :tracks, :exclusive_wave_price, :integer
    add_column :tracks, :is_non_exclusive_wave, :boolean
    add_column :tracks, :non_exclusive_wave_price, :integer
  end
end
