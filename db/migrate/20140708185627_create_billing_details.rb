class CreateBillingDetails < ActiveRecord::Migration
  def change
    create_table :billing_details do |t|
      t.integer :user_id
      t.text :first_name
      t.text :last_name
      t.text :artist_name
      t.text :country
      t.text :email
      t.boolean :create_user
      t.integer :cart_id
      t.timestamps
    end
  end
end
