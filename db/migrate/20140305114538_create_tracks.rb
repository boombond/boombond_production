class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :title
      t.string :bpm
      t.string :genere
      t.string :subgenere
      t.string :tags
      t.string :key
      t.string :mood1
      t.string :mood2
      t.text :comment
      t.integer :promotion_licence_price
      t.integer :promotion_tpt_price
      t.integer :exclusive_licence_price
      t.integer :exclusive_tpt_price
      t.integer :profit_licence_price
      t.integer :profit_tpt_price
      t.integer :user_id
      t.integer :download_count
      t.integer :published
      t.boolean :active

      t.timestamps
    end
  end
end
