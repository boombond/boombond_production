class AddOrderIdToCart < ActiveRecord::Migration
  def change
    add_column :carts, :order_id, :text
  end
end
