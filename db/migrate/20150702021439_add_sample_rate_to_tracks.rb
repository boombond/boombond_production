class AddSampleRateToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :sample_rate, :string
  end
end
