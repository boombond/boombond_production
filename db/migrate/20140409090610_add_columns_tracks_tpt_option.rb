class AddColumnsTracksTptOption < ActiveRecord::Migration
  def change
  	add_column :tracks, :track_promotion, :boolean
  	add_column :tracks, :tpt_promotion, :boolean
  	add_column :tracks, :track_exclusive, :boolean
  	add_column :tracks, :tpt_exclusive, :boolean
  	add_column :tracks, :track_profit, :boolean
  	add_column :tracks, :tpt_profit, :boolean
  end
end
