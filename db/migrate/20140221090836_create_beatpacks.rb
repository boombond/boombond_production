class CreateBeatpacks < ActiveRecord::Migration
  def change
    create_table :beatpacks do |t|
      t.string 	:Title
      t.integer :Trackscount
      t.text 	:Comment
      t.integer :Downloadcount
      t.integer :user_id
      t.timestamps
    end
  end
end
