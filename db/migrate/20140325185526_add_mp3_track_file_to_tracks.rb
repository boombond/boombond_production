class AddMp3TrackFileToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :mp3_track_file_file_name, :string
    add_column :tracks, :mp3_track_file_content_type, :string
    add_column :tracks, :mp3_track_file_file_size, :integer
    add_column :tracks, :mp3_track_file_updated_at, :datetime
  end
end
