class AddDownloadCountToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :wav_download_count,:integer
    add_column :tracks, :tpt_download_count,:integer
  end
end
