class AddArchiveAtToGlacierDetails < ActiveRecord::Migration
  def change
    add_column :glacier_details, :archive_at, :datetime
  end
end
