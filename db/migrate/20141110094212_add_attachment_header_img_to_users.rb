class AddAttachmentHeaderImgToUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :header_img
    end
  end

  def self.down
    remove_attachment :users, :header_img
  end
end
