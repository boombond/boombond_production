class AddIndexToTracksAndUsers < ActiveRecord::Migration
  def change
    add_index :tracks, :title
    add_index :tracks, :tags
    add_index :users, :Firstname
    add_index :users, :Lastname
    add_index :users, :ArtistName
  end
end
