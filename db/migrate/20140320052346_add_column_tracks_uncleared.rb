class AddColumnTracksUncleared < ActiveRecord::Migration
  def change
  	add_column :tracks, :unclearedsample,:string

	add_column :tracks, :unclearedsong,:string
	add_column :tracks, :unclearedartist,:string
	add_column :tracks, :uncleredDescription,:string

	add_column :tracks, :unclearedsong1,:string
	add_column :tracks, :unclearedartist1,:string
	add_column :tracks, :uncleredDescription1,:string

	add_column :tracks, :unclearedsong2,:string
	add_column :tracks, :unclearedartist2,:string
	add_column :tracks, :uncleredDescription2,:string

	add_column :tracks, :unclearedsong3,:string
	add_column :tracks, :unclearedartist3,:string
	add_column :tracks, :uncleredDescription3,:string

	add_column :tracks, :unclearedsong4,:string
	add_column :tracks, :unclearedartist4,:string
	add_column :tracks, :uncleredDescription4,:string


  end
end
