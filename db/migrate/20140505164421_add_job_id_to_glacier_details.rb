class AddJobIdToGlacierDetails < ActiveRecord::Migration
  def change
    add_column :glacier_details, :job_id, :text
  end
end
