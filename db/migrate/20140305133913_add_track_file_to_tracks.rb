class AddTrackFileToTracks < ActiveRecord::Migration
  def change
  	add_column :tracks, :track_file_file_name, :string
    add_column :tracks, :track_file_content_type, :string
    add_column :tracks, :track_file_file_size, :integer
    add_column :tracks, :track_file_updated_at, :datetime

    add_column :tracks, :tpt_file_name, :string
    add_column :tracks, :tpt_content_type, :string
    add_column :tracks, :tpt_file_size, :integer
    add_column :tracks, :tpt_updated_at, :datetime
  end
end
