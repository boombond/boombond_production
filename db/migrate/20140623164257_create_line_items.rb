class CreateLineItems < ActiveRecord::Migration
  def self.up
    create_table :line_items do |t|
      t.decimal :unit_price
      t.integer :track_id
      t.integer :cart_id
      t.integer :quantity
      t.string :item_type
      t.timestamps
    end
  end
  
  def self.down
    drop_table :line_items
  end
end