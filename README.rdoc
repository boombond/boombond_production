Boombond

www.boombond.com

Requirements: 
              Lame encoder
              AWS IAM details
              Database
              Rails4
              Ruby2
              Ubuntu os

steps to execute this application.

1. git clone repo

2. bundle install

3. edit database.yml

4. rake db:create

5. rake db:migrate

6. rake db:seed

7. rails s # to start web server

8. open new tab with app path run background jobs with command

   rake jobs:work

9. you need aws crdentials to store wav and tpt(zip) files and these files store in glacier with mp3 extention.

for this convertion wav to mp3 convertion you need to install lame encoder in ubuntu os.

 command to install lame: "sudo apt-get install lame libmp3lame0"

10. Thank you
