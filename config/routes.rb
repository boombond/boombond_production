Beatbucks::Application.routes.draw do
  
     
    resources :payment_notifications

    resources :line_items

  resources :carts do
    member do
      get :checkout
      get :billing
      post :save_billing
      get :download
    end
    collection do
      get :current
      get :billing_payments
      get :billing_payments2
      get :billing_payments3
      post :complete_payment
      get :payment_done
      get :license_agrement
      post :add_to_cart
      
      get :latest_pending_cart
      get :latest_pending_line_items
    end
    end

   resources :glacier_details

   devise_for :users, controllers: { omniauth_callbacks: "omniauth_callbacks", :registrations => "registrations" }
  as :user do
    get "settings" => "users#settings"
    get "social_networks" => "users#social_networks"
    put "users/:id/save_settings" => "users#save_settings"
    get "/profile" => "registrations#edit"
  end
  #map.current_cart 'cart', :controller => 'carts', :action => 'show', :id => 'current'
   get "/users/index"
   get "/registrations/update"
   post "/registrations/update"
   get "/home/state_cities"
   get "/home/state_codes"
   get "/dashbord" => "home#dashbord"
   get "/edit" => "users#edit"
   post "/tracks/download_zip" => "tracks#download_zip"
   post "/home/settings"
   get "/home/sub_genres"
   get "/beatpacks" => "beatpacks#new"
   get "/earnings" => "producers#earnings"

    get "/buy" => "users#buy"
    get "/license" => "users#license"
    get "/music" => "users#music"
    get "/files" => "users#files"
  get "/descripition" => "users#descripition"
  get "/author_profile/:id" => "users#author_profile"
  
  get "/orders" => "users#orders"
  get "/about_us" => "home#about_us"
  get "global_search" => "users#global_search"
  put "set_song_in_session" => "users#set_song_in_session"

    resources :tracks do
      member do
        get :download
        post :upload_tpt_file
        post :publish
        get :new_track_step2
        get :mp3_download
        get :download_zip
        get :download_wav
        get :download_tpt
        get :increment_play_count
      end
    
    collection do
      post :upload_trak_file
      get :upload_trak_file
      get :license
      get :new1
    end
    end
    resources :beatpacks 
  
  
  root 'users#index'  
  

end
