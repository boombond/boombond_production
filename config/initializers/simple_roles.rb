SimpleRoles.configure do
  valid_roles :music_lover,:others,:singer,:dj,:producer_or_beat_maker_or_composer,:admin
  #"MusicLover", "DJ", "Singer", "Others", "Producer\nBeatMaker\nComposer"
  strategy :many # Default is :one
end