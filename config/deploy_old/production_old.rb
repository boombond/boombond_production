# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, %w{deployer@104.236.227.19}
role :web, %w{deployer@104.236.227.19}
role :db,  %w{deployer@104.236.227.19}



set :stage, :production

set :access_key_id, 'AKIAJXJIEWZBXESLFSLQ'
set :secret_access_key, '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'
set :stack_id, '04fcb1c3-ce6b-4459-a513-939774ef2d09'
set :app_id, '0f2ca7d8-55af-403b-a5e7-dd2ffd1e5f65'
#set :opsworks_custom_json, '<opsworks_custom_json>'



# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server '104.236.227.19', user: 'deployer', roles: %w{web app}, my_property: :my_value


# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
  set :ssh_options, {
    keys: %w(/home/gopi/Desktop/beatbucket/BoomBond-Key.pem/.ssh/id_rsa),
    forward_agent: false,
    auth_methods: %w(password)
  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
